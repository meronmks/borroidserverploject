package com.meronmks.botroidserver.app;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by meronmks on 2015/02/01.
 */
public class MentionSettingActivity extends Activity implements View.OnClickListener {

    private android.widget.ListView lv;
    private SQLiteDatabase mydb;
    private MySQLiteOpenHelper hlpr;
    private EditText priorityEditText,ｗordEditText,mentionEditText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mention_setting_activity);
        lv = (android.widget.ListView)findViewById(R.id.menWordListView);
        //DB読み込み準備
        hlpr = new MySQLiteOpenHelper(this);
        mydb = hlpr.getWritableDatabase();
        //全文検索
        Cursor cursor = mydb.query("mention", null, null, null, null, null, null);
        int rowcount = cursor.getCount();
        if (cursor.moveToFirst()) {
            MentionAdapter adapter = new MentionAdapter(this);
            for (int i = 0; i < rowcount; i++) {
                MentionVariable tmp = new MentionVariable();
                tmp.id = cursor.getString(0);
                tmp.priority = cursor.getString(1);
                tmp.receptionｗord = cursor.getString(2);
                tmp.replyｗord = cursor.getString(3);
                adapter.add(tmp);
                cursor.moveToNext();
            }
            lv.setAdapter(adapter);
        }
        priorityEditText = (EditText)findViewById(R.id.priorityEditText);
        ｗordEditText = (EditText)findViewById(R.id.ｗordEditText);
        mentionEditText = (EditText)findViewById(R.id.mentionEditText);
        Button addButton = (Button)findViewById(R.id.mentonAddButton);
        addButton.setOnClickListener(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //DBを閉じる
        mydb.close();
        hlpr.close();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mentonAddButton:
                ContentValues values = new ContentValues();
                SpannableStringBuilder sp = (SpannableStringBuilder)priorityEditText.getText();
                SpannableStringBuilder sp2 = (SpannableStringBuilder)ｗordEditText.getText();
                SpannableStringBuilder sp3 = (SpannableStringBuilder)mentionEditText.getText();
                values.put("priority", sp.toString());
                values.put("receptionｗord", sp2.toString());
                values.put("replyｗord", sp3.toString());
                //DB書き込み
                mydb.insert("mention", null, values);
                break;
        }
    }
}
