package com.meronmks.botroidserver.app;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by meronmks on 2015/02/01.
 */
public class UserInfoAdapter extends ArrayAdapter<UserInfoVariable> {
    private LayoutInflater mInflater;

    public UserInfoAdapter(Context context) {
        super(context, android.R.layout.simple_list_item_1);
        mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item_userinfo, null);
        }
        UserInfoVariable item = getItem(position);
        TextView name = (TextView) convertView.findViewById(R.id.recrptionWordTextView);
        name.setText("ScreenName:" + item.name);
        TextView id = (TextView) convertView.findViewById(R.id.idTextView);
        id.setText("ID:" + item.id);
        TextView twitterid = (TextView) convertView.findViewById(R.id.priorityTextView);
        twitterid.setText("TwitterID:" + item.twitterid);
        return convertView;
    }
}
