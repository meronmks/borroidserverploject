package com.meronmks.botroidserver.app;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by meronmks on 2015/02/01.
 */
public class MentionAdapter extends ArrayAdapter<MentionVariable> {
    private LayoutInflater mInflater;

    public MentionAdapter(Context context) {
        super(context, android.R.layout.simple_list_item_1);
        mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item_mention, null);
        }
        MentionVariable item = getItem(position);
        TextView receptionｗord = (TextView) convertView.findViewById(R.id.recrptionWordTextView);
        receptionｗord.setText("反応ワード:" + item.receptionｗord);
        TextView id = (TextView) convertView.findViewById(R.id.idTextView);
        id.setText("ID:" + item.id);
        TextView priority = (TextView) convertView.findViewById(R.id.priorityTextView);
        priority.setText("優先度:" + item.priority);
        TextView replyｗord = (TextView) convertView.findViewById(R.id.replyWordＴextView);
        replyｗord.setText("返信ワード:" + item.replyｗord);
        return convertView;
    }
}
