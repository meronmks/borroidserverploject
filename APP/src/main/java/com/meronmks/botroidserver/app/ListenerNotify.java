package com.meronmks.botroidserver.app;

import android.app.ListActivity;

/**
 * Created by meronmks on 2015/02/02.
 */
public class ListenerNotify {
    private MyListenerInterface listener = null;

    public void ListenerNotify(){

    }

    public void sendmention(long tweetID,String sentText){
        listener.onMentonListener(tweetID,sentText);
    }

    public void sendLogText(String sendText){
        listener.onPutLogText(sendText);
    }

    public void sendDirectMessage(long sendID,String sendText){
        listener.onSendDirectMessage(sendID,sendText);
    }

    public void sendTweet(String sendText){
        listener.onSendTweet(sendText);
    }

    /**
     * リスナーを追加する
     * @param listener
     */
    public void setListener(MyListenerInterface listener){
        this.listener = listener;
    }

    /**
     * リスナーを削除する
     */
    public void removeListener(){
        this.listener = null;
    }
}
