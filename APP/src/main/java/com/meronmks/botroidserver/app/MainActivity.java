package com.meronmks.botroidserver.app;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.content.Intent;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.os.Process;
import twitter4j.*;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.content.ContentValues;
import android.content.*;

import java.util.Calendar;
import java.util.Random;

public class MainActivity extends Activity implements MyListenerInterface {
    private Twitter mTwitter;
    private TwitterStream twitterStream;
    private MyUserStreamAdapter mMyUserStreamAdapter;
    private TextView logText;
    private long MyID;
    private String MyScreenName;
    public static ListenerNotify ln;
    private Handler handler;
    private float batteryLevel = 0.0f;

    /**
     * 初期設定
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        logText = (TextView)findViewById(R.id.logTextView);
        //通知用クラスのインスタンス
        ln = new ListenerNotify();
        //通知用クラスに通知先のインスタンス付加
        ln.setListener(this);
        //Handler用意
        handler = new Handler();
        //バッテリー残量等の取得準備
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
        registerReceiver(broadcastReceiver, intentFilter);
        setAlarm();
        createAPIKey();
        Twetterini();
        startStream();
    }

    /**
     * リスナー動作登録
     */
    @Override
    public void onMentonListener(long tweetID,String sendText) {
        SendMention(tweetID, sendText);
    }

    @Override
    public void onPutLogText(final String sendText) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                logText.append(sendText);
                logText.refreshDrawableState();
            }
        });
    }

    @Override
    public void onSendDirectMessage(long sendID, String sendText) {
        SendDM(sendID,sendText);
    }

    @Override
    public void onSendTweet(String sendtext) {
        SendTweet(sendtext);
    }

    /**
     * 終了時動作
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopStream();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_tlsettings:
                return true;
            case R.id.action_mensettings:
                Intent mentionsetting = new Intent(MainActivity.this,MentionSettingActivity.class);
                startActivity(mentionsetting);
                return true;
            case R.id.action_usersettings:
                Intent usersetting = new Intent(MainActivity.this,UserSettingActivity.class);
                startActivity(usersetting);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void createAPIKey(){
        //一般設定
        ConfigurationBuilder builder = new ConfigurationBuilder();
        {
            // Twitter4Jに対してOAuth情報を設定
            // アプリ固有の情報
            builder.setOAuthConsumerKey("");
            builder.setOAuthConsumerSecret("");
            // アプリ＋ユーザー固有の情報
            builder.setOAuthAccessToken("");
            builder.setOAuthAccessTokenSecret("");

            //HTTPタイムアウト設定(ミリ秒)
            builder.setHttpConnectionTimeout(10000);
        }
        ln.sendLogText("初期設定完了\n");
        // 1. TwitterStreamFactory をインスタンス化する
        Configuration conf = builder.build();
        TwitterStreamFactory twitterStreamFactory = new TwitterStreamFactory(conf);
        // 2. TwitterStream をインスタンス化する
        twitterStream = twitterStreamFactory.getInstance();
        //TwitterFactoryをインスタンス化する
        TwitterFactory twitterFactory = new TwitterFactory(conf);
        //Twitterをインスタンス化する
        mTwitter = twitterFactory.getInstance();
        //ストリーム受信時の動作クラスをインスタンス化
        mMyUserStreamAdapter = new MyUserStreamAdapter();
        ln.sendLogText("streaming準備完了\n");
    }

    /**ストリーミング開始**/
    private void startStream(){
        // 4. TwitterStream に UserStreamListener を実装したインスタンスを設定する
        twitterStream.addListener(mMyUserStreamAdapter);
        // 5. TwitterStream#user() を呼び出し、ユーザーストリームを開始する
        twitterStream.user();
        ln.sendLogText("streamingを開始します\n");
    }

    /**ストリーミング終了**/
    private void stopStream() {
        if(twitterStream != null){
            twitterStream.shutdown();
            ln.sendLogText("streamingを終了します\n");
        }
    }

    /**ストリーミングの動作設定**/
    public class MyUserStreamAdapter extends UserStreamAdapter {

        //例外発生時
        @Override
        public void onException(Exception e) {
            ln.sendLogText("Streamにてエラー発生\n");
            ln.sendLogText(e.getMessage() + "\n");
            Log.d("BotSystemMessage",e.getMessage());
        }
        //ツイート受信時の動作
        @Override
        public void onStatus(final Status status) {
            ln.sendLogText("TweetID:" + status.getId() + " " + status.getUser().getName() + "(@" + status.getUser().getScreenName() + ")" + "\n");
            ln.sendLogText("ツイート内容:" + status.getText() + "\n");
            ln.sendLogText("時間:" + status.getCreatedAt() + "\n");
            Boolean skipFlag = false;
            //DB書き込み準備
            MySQLiteOpenHelper hlpr = new MySQLiteOpenHelper(MainActivity.this);
            SQLiteDatabase mydb = hlpr.getWritableDatabase();
            //書き込むユーザがDB内に存在するか検索
            String text = "twitterid=" + status.getUser().getId();
            Cursor cursor = mydb.query("user",null,text,null,null,null,null);
            if(!cursor.moveToFirst() && !status.getUser().getScreenName().equals(MyScreenName)){
                //見つからなかったらかつ自分じゃなかったら書き込むデータ作成
                ln.sendLogText(status.getUser().getName() + "さんをDBに登録します\n");
                ContentValues values = new ContentValues();
                values.put("twitterid", status.getUser().getId());
                values.put("userid", status.getUser().getScreenName());
                values.put("name", status.getUser().getName());
                values.put("love", "0");
                values.put("hate", "0");
                values.put("nickname", status.getUser().getName());
                //DB書き込み
                mydb.insert("user", null, values);
            }

            if(status.getRetweetedStatus() == null) {
                /**@返信処理**/
                boolean RepNotifFlag = false;
                for (UserMentionEntity UrlLink : status.getUserMentionEntities()) {
                    if (UrlLink.getScreenName().equals(MyScreenName)) {
                        RepNotifFlag = true;
                    }
                }
                if (RepNotifFlag) {
                    Log.d("BotSystemMessage", "RepFound");
                    mydb.close();
                    mydb = hlpr.getWritableDatabase();
                    Cursor cursor2 = mydb.query("mention", null, null, null, null, null, null);
                    int rowcount = cursor2.getCount();
                    if (cursor2.moveToFirst()) {
                        for (int i = 0; i < rowcount; i++) {
                            String tmp = cursor2.getString(2);
                            if (status.getText().matches(tmp)) {
                                cursor2.close();
                                mydb.close();
                                mydb = hlpr.getWritableDatabase();
                                tmp = "receptionword='" + tmp + "'";
                                cursor2 = mydb.query("mention", null, tmp, null, null, null, null);
                                cursor2.moveToFirst();
                                Random rnd = new Random();
                                int ran = rnd.nextInt(cursor2.getCount());
                                for(int j = 0; j < ran; j++){
                                    cursor2.moveToNext();
                                }
                                ln.sendmention(status.getId(), "@" + status.getUser().getScreenName() + " " + cursor2.getString(3));
                                ln.sendLogText(status.getUser().getName() + "さんへDB内容で返信しました\n");
                                skipFlag = true;
                                break;
                            }
                            cursor2.moveToNext();
                        }
                        //この時点でskipFlagがfalseならDBに該当なしだった
                        if(!skipFlag){
                            //乱数作成部
                            ln.sendLogText(status.getUser().getName() + "さんへの返信内容が見つかりませんでした\n");
                        }
                    }
                }

                /**TLから抽出**/
                if (!status.getUser().getScreenName().equals(MyScreenName) && status.getUserMentionEntities().length == 0) {
                    mydb.close();
                    mydb = hlpr.getWritableDatabase();
                    Cursor cursor3 = mydb.query("timeline", null, null, null, null, null, null);
                    int rowcount = cursor3.getCount();
                    if (cursor3.moveToFirst() && !skipFlag) {
                        for (int i = 0; i < rowcount; i++) {
                            String tmp = cursor3.getString(2);
                            if (status.getText().matches(tmp)) {
                                Log.d("BotSystemMessage", "TLBDFound");
                                cursor3.close();
                                mydb.close();
                                mydb = hlpr.getWritableDatabase();
                                tmp = "receptionword='" + tmp + "'";
                                cursor3 = mydb.query("timeline", null, tmp, null, null, null, null);
                                cursor3.moveToFirst();
                                Random rnd = new Random();
                                int ran = rnd.nextInt(cursor3.getCount());
                                for(int j = 0; j < ran; j++){
                                    cursor3.moveToNext();
                                }
                                tmp = cursor3.getString(3);
                                tmp = tmp.replaceAll("%n", status.getUser().getName());    //文字置換
                                ln.sendmention(status.getId(), "@" + status.getUser().getScreenName() + " " + tmp);
                                ln.sendLogText(status.getUser().getName() + "さんのつぶやきに反応しました\n");
                                skipFlag = true;
                                break;
                            }
                            cursor3.moveToNext();
                        }
                    }
                }
            }
            //DB関連を閉じる
            mydb.close();
            hlpr.close();
            ln.sendLogText("\n");
        }

        //DMを受け取った時
        @Override
        public void onDirectMessage(DirectMessage DM) {
            if("info".equals(DM.getText())){
                long uptime = SystemClock.uptimeMillis();
                String SendText = "起動から" + (uptime / 1000 / 60 / 60) + "時間:" + (uptime / 1000 / 60 % 60) +
                                    "分\n電池残量" + batteryLevel + "%";
                ln.sendDirectMessage(DM.getSender().getId(),SendText);
                ln.sendLogText("DMにてinfoを確認。情報を送信します");
            }
            if("stop".equals(DM.getText())){
                Log.d("BotSystemMessage","stopを検知終了します");
                Process.killProcess(Process.myPid());
            }
        }
    }

    private void SendMention(final long tweetID,final String sentText) {
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                StatusUpdate update = new StatusUpdate(sentText);
                update.inReplyToStatusId(tweetID);
                try {
                    mTwitter.updateStatus(update);
                } catch (TwitterException e) {
                    e.printStackTrace();
                    Log.d("BotSystemMessage",e.getMessage());
                }
                return null;
            }
        };
        task.execute();
    }

    private void SendDM(final long sendID, final String sendText){
        AsyncTask<Void,Void,Void> task = new AsyncTask<Void, Void, Void>() {
            String messege;
            @Override
            protected Void doInBackground(Void... params) {

                try {
                    mTwitter.sendDirectMessage(sendID,sendText);
                } catch (TwitterException e) {
                    e.printStackTrace();
                    messege = e.getMessage();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if(messege != null){
                    ln.sendLogText(messege + "\n");
                }
            }
        };
        task.execute();
    }

    private void SendTweet(final String sentText) {
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                StatusUpdate update = new StatusUpdate(sentText);
                try {
                    mTwitter.updateStatus(update);
                } catch (TwitterException e) {
                    e.printStackTrace();
                    Log.d("BotSystemMessage",e.getMessage());
                }
                return null;
            }
        };
        task.execute();
    }

    private void Twetterini(){
        AsyncTask<Void, Void, User> task = new AsyncTask<Void, Void, User>() {
            @Override
            protected User doInBackground(Void... params) {
                try {
                    return mTwitter.verifyCredentials();//Userオブジェクトを作成
                } catch (TwitterException e) {
                    e.printStackTrace();
                    Log.d("BotSystemMessage",e.getMessage());
                }
                return null;
            }

            @Override
            protected void onPostExecute(User result) {
                if(result != null){
                    MyID = result.getId();
                    MyScreenName = result.getScreenName();
                    ln.sendLogText("IDおよびScreenName取得完了\n");
                }else{
                    ln.sendLogText("IDおよびScreenName取得に失敗しました\n");
                }
            }
        };
        task.execute();
    }

    /**
     * 定期ツイートセット
     */
    public void setAlarm(){
        //時間格納変数
        long alarmSrtTime;
        //送信テキスト格納変数
        String sendText = "";
        // 通知させたい時間をCalendarを使って定義する
        Calendar calSet = Calendar.getInstance();
        Calendar caltmp = Calendar.getInstance();
        calSet.setTimeInMillis(System.currentTimeMillis());
        calSet.setTimeZone(java.util.TimeZone.getDefault());
        caltmp.set(Calendar.SECOND,0);
        caltmp.set(Calendar.MILLISECOND,0);
        //DB読み込み準備
        MySQLiteOpenHelper hlpr = new MySQLiteOpenHelper(MainActivity.this);
        SQLiteDatabase mydb = hlpr.getReadableDatabase();
        String text = "sethour >= " + calSet.get(Calendar.HOUR_OF_DAY);
        Cursor cursor = mydb.query("routine", null, text, null, null, null, "sethour ASC, setminute ASC");
        if(cursor.moveToFirst()){
            caltmp.set(Calendar.HOUR_OF_DAY, cursor.getInt(1));
            caltmp.set(Calendar.MINUTE, cursor.getInt(2));
            caltmp.setTimeZone(java.util.TimeZone.getDefault());
            int count = cursor.getCount();
            if(count == 1) {
                if(caltmp.getTimeInMillis() >= calSet.getTimeInMillis()) {
                    calSet.set(Calendar.HOUR_OF_DAY, cursor.getInt(1));
                    calSet.set(Calendar.MINUTE, cursor.getInt(2));
                    calSet.set(Calendar.SECOND,0);
                    calSet.set(Calendar.MILLISECOND,0);
                    alarmSrtTime = calSet.getTimeInMillis();
                }else{
                    cursor.close();
                    mydb.close();
                    mydb = hlpr.getReadableDatabase();
                    cursor = mydb.query("routine", null, null, null, null, null, "sethour ASC, setminute ASC");
                    cursor.moveToFirst();
                    calSet.set(Calendar.HOUR_OF_DAY, cursor.getInt(1));
                    calSet.set(Calendar.MINUTE, cursor.getInt(2));
                    calSet.set(Calendar.SECOND,0);
                    calSet.set(Calendar.MILLISECOND,0);
                    alarmSrtTime = calSet.getTimeInMillis() + 24*60*60*1000;
                }
                sendText = cursor.getString(3);
            }else{
                Boolean flag = false;
                for(int i = 0; i < count; i++){
                    if(caltmp.getTimeInMillis() >= calSet.getTimeInMillis() && flag == false) {
                        calSet.set(Calendar.HOUR_OF_DAY, cursor.getInt(1));
                        calSet.set(Calendar.MINUTE, cursor.getInt(2));
                        sendText = cursor.getString(3);
                        flag = true;
                    }else{
                        cursor.moveToNext();
                        caltmp.set(Calendar.HOUR_OF_DAY, cursor.getInt(1));
                        caltmp.set(Calendar.MINUTE, cursor.getInt(2));
                        caltmp.setTimeZone(java.util.TimeZone.getDefault());
                    }
                }
                calSet.set(Calendar.SECOND,0);
                calSet.set(Calendar.MILLISECOND,0);
                alarmSrtTime = calSet.getTimeInMillis();
                if(!flag){
                    cursor.close();
                    mydb.close();
                    mydb = hlpr.getReadableDatabase();
                    cursor = mydb.query("routine", null, null, null, null, null, "sethour ASC, setminute ASC");
                    cursor.moveToFirst();
                    calSet.set(Calendar.HOUR_OF_DAY, cursor.getInt(1));
                    calSet.set(Calendar.MINUTE, cursor.getInt(2));
                    sendText = cursor.getString(3);
                    calSet.set(Calendar.SECOND,0);
                    calSet.set(Calendar.MILLISECOND,0);
                    alarmSrtTime = calSet.getTimeInMillis() + 24*60*60*1000;
                }
            }
        }else{
            cursor.close();
            mydb.close();
            mydb = hlpr.getReadableDatabase();
            cursor = mydb.query("routine", null, null, null, null, null, "sethour ASC, setminute ASC");
            cursor.moveToFirst();
            calSet.set(Calendar.HOUR_OF_DAY, cursor.getInt(1));
            calSet.set(Calendar.MINUTE, cursor.getInt(2));
            calSet.set(Calendar.SECOND,0);
            calSet.set(Calendar.MILLISECOND,0);
            alarmSrtTime = calSet.getTimeInMillis() + 24*60*60*1000;
            sendText = cursor.getString(3);
        }
        //定期ツイートセット準備
        Intent i = new Intent(MainActivity.this, AlarmReceiver.class); // ReceivedActivityを呼び出すインテントを作成
        // レシーバーへ送信テキスト送信
        i.putExtra("sendtext", sendText);
        i.setType(sendText);
        PendingIntent sender = PendingIntent.getBroadcast(MainActivity.this, 0, i, 0); // ブロードキャストを投げるPendingIntentの作成
        
        AlarmManager alarm = (AlarmManager)getSystemService(ALARM_SERVICE); // AlramManager取得
        alarm.set(AlarmManager.RTC_WAKEUP, alarmSrtTime, sender); // AlramManagerにPendingIntentを登録
        mydb.close();
        hlpr.close();
        Log.d("BotSystemMessage", "Timerを" + calSet.get(Calendar.HOUR_OF_DAY) + "時" + calSet.get(Calendar.MINUTE) + "分にセットしました");
        Log.d("BotSystemMessage","送信予定のテキストは「" + sendText + "」です");
    }

    //ブロードキャストレシーバーのインスタンス化
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_BATTERY_CHANGED)) {
                int level = intent.getIntExtra("level", 0);
                int scale = intent.getIntExtra("scale", 0);
                batteryLevel = ((float)level / (float)scale * 100);
            }
        }
    };
}
