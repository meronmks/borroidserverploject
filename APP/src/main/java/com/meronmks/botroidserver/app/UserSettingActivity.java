package com.meronmks.botroidserver.app;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

/**
 * Created by meronmks on 2015/02/01.
 */
public class UserSettingActivity extends Activity {
    private android.widget.ListView lv;
    private SQLiteDatabase mydb;
    private MySQLiteOpenHelper hlpr;
    //userテーブル内すべての要素を見る
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_setting_activity);
        lv = (android.widget.ListView)findViewById(R.id.userListView);
        //DB読み込み準備
        hlpr = new MySQLiteOpenHelper(this);
        mydb = hlpr.getWritableDatabase();
        //全文検索
        Cursor cursor = mydb.query("user",null,null,null,null,null,null);
        int rowcount = cursor.getCount();
        if(cursor.moveToFirst()) {
            UserInfoAdapter adapter = new UserInfoAdapter(this);
            for (int i = 0; i < rowcount ; i++) {
                UserInfoVariable tmp = new UserInfoVariable();
                tmp.id = cursor.getString(0);
                tmp.twitterid = cursor.getString(1);
                tmp.name = cursor.getString(2);
                adapter.add(tmp);
                cursor.moveToNext();
            }
            lv.setAdapter(adapter);
        }
        //DBを閉じる
        mydb.close();
        hlpr.close();
    }
}
