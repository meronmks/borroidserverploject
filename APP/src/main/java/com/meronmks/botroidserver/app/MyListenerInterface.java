package com.meronmks.botroidserver.app;

import java.util.EventListener;

/**
 * Created by meronmks on 2015/02/02.
 */
public interface MyListenerInterface extends EventListener {
    /**
     * リプライを受信し、条件に合ったツイートだったのを通知する
     */
    public void onMentonListener(long tweetID,String sendText);
    /**
     * Logに文字を追加する
     */
    public void onPutLogText(String sendText);
    /**
     * DMを送信する
     */
    public void onSendDirectMessage(long sendID,String sendText);
    /**
     * ツイートを送信する
     */
    public void onSendTweet(String sendtext);
}
