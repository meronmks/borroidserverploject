package com.meronmks.botroidserver.app;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;
import twitter4j.*;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

import java.util.Calendar;

/**
 * Created by meronmks on 2015/02/11.
 */
public class AlarmReceiver extends BroadcastReceiver {
    private Twitter mTwitter;
    private String sendText;
    @Override
    public void onReceive(Context context, Intent intent) {
        sendText = intent.getStringExtra("sendtext");
        Log.d("BotSystemMessage", sendText + "を送信しました");
        createAPIKey();
        SendTweet(sendText);
        setAlarm(context);
    }

    /**
     * 定期ツイートセット
     */
    public void setAlarm(Context context){
        //時間格納変数
        long alarmSrtTime;
        // 通知させたい時間をCalendarを使って定義する
        Calendar calSet = Calendar.getInstance();
        Calendar caltmp = Calendar.getInstance();
        calSet.setTimeInMillis(System.currentTimeMillis());
        calSet.setTimeZone(java.util.TimeZone.getDefault());
        caltmp.set(Calendar.SECOND,0);
        caltmp.set(Calendar.MILLISECOND,0);
        //DB読み込み準備
        MySQLiteOpenHelper hlpr = new MySQLiteOpenHelper(context);
        SQLiteDatabase mydb = hlpr.getReadableDatabase();
        String text = "sethour >= " + calSet.get(Calendar.HOUR_OF_DAY);
        Cursor cursor = mydb.query("routine", null, text, null, null, null, "sethour ASC, setminute ASC");
        if(cursor.moveToFirst()){
            caltmp.set(Calendar.HOUR_OF_DAY, cursor.getInt(1));
            caltmp.set(Calendar.MINUTE, cursor.getInt(2));
            caltmp.setTimeZone(java.util.TimeZone.getDefault());
            int count = cursor.getCount();
            if(count == 1) {
                if(caltmp.getTimeInMillis() >= calSet.getTimeInMillis()) {
                    calSet.set(Calendar.HOUR_OF_DAY, cursor.getInt(1));
                    calSet.set(Calendar.MINUTE, cursor.getInt(2));
                    calSet.set(Calendar.SECOND,0);
                    calSet.set(Calendar.MILLISECOND,0);
                    alarmSrtTime = calSet.getTimeInMillis();
                }else{
                    cursor.close();
                    mydb.close();
                    mydb = hlpr.getReadableDatabase();
                    cursor = mydb.query("routine", null, null, null, null, null, "sethour ASC, setminute ASC");
                    cursor.moveToFirst();
                    calSet.set(Calendar.HOUR_OF_DAY, cursor.getInt(1));
                    calSet.set(Calendar.MINUTE, cursor.getInt(2));
                    calSet.set(Calendar.SECOND,0);
                    calSet.set(Calendar.MILLISECOND,0);
                    alarmSrtTime = calSet.getTimeInMillis() + 24*60*60*1000;
                }
                sendText = cursor.getString(3);
            }else{
                Boolean flag = false;
                for(int i = 0; i < count; i++){
                    if(caltmp.getTimeInMillis() >= calSet.getTimeInMillis() && flag == false) {
                        calSet.set(Calendar.HOUR_OF_DAY, cursor.getInt(1));
                        calSet.set(Calendar.MINUTE, cursor.getInt(2));
                        flag = true;
                        sendText = cursor.getString(3);
                    }else{
                        cursor.moveToNext();
                        caltmp.set(Calendar.HOUR_OF_DAY, cursor.getInt(1));
                        caltmp.set(Calendar.MINUTE, cursor.getInt(2));
                        caltmp.setTimeZone(java.util.TimeZone.getDefault());
                    }
                }
                calSet.set(Calendar.SECOND,0);
                calSet.set(Calendar.MILLISECOND,0);
                alarmSrtTime = calSet.getTimeInMillis();
                if(!flag){
                    cursor.close();
                    mydb.close();
                    mydb = hlpr.getReadableDatabase();
                    cursor = mydb.query("routine", null, null, null, null, null, "sethour ASC, setminute ASC");
                    cursor.moveToFirst();
                    calSet.set(Calendar.HOUR_OF_DAY, cursor.getInt(1));
                    calSet.set(Calendar.MINUTE, cursor.getInt(2));
                    sendText = cursor.getString(3);
                    calSet.set(Calendar.SECOND,0);
                    calSet.set(Calendar.MILLISECOND,0);
                    alarmSrtTime = calSet.getTimeInMillis() + 24*60*60*1000;
                }
            }
        }else{
            cursor.close();
            mydb.close();
            mydb = hlpr.getReadableDatabase();
            cursor = mydb.query("routine", null, null, null, null, null, "sethour ASC, setminute ASC");
            cursor.moveToFirst();
            calSet.set(Calendar.HOUR_OF_DAY, cursor.getInt(1));
            calSet.set(Calendar.MINUTE, cursor.getInt(2));
            calSet.set(Calendar.SECOND,0);
            calSet.set(Calendar.MILLISECOND,0);
            sendText = cursor.getString(3);
            alarmSrtTime = calSet.getTimeInMillis() + 24*60*60*1000;
        }
        //定期ツイートセット準備
        Intent i = new Intent(context, AlarmReceiver.class); // ReceivedActivityを呼び出すインテントを作成
        // レシーバーへ送信テキスト送信
        i.putExtra("sendtext", sendText);
        i.setType(sendText);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, i, 0); // ブロードキャストを投げるPendingIntentの作成
        AlarmManager alarm = (AlarmManager)context.getSystemService(context.ALARM_SERVICE); // AlramManager取得
        alarm.set(AlarmManager.RTC_WAKEUP, alarmSrtTime, sender); // AlramManagerにPendingIntentを登録
        mydb.close();
        hlpr.close();
        Log.d("BotSystemMessage","Timerを" + calSet.get(Calendar.HOUR_OF_DAY) + "時" + calSet.get(Calendar.MINUTE) + "分にセットしました");
        Log.d("BotSystemMessage","送信予定のテキストは「" + sendText + "」です");
    }

    private void SendTweet(final String SendText) {
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    mTwitter.updateStatus(SendText);
                } catch (TwitterException e) {
                    e.printStackTrace();
                    Log.d("BotSystemMessage",e.getMessage());
                }
                return null;
            }
        };
        task.execute();
    }

    private void createAPIKey(){
        //一般設定
        ConfigurationBuilder builder = new ConfigurationBuilder();
        {
            // Twitter4Jに対してOAuth情報を設定
            // アプリ固有の情報
            builder.setOAuthConsumerKey("HiAturH945UNqzQShMJLDhVDo");
            builder.setOAuthConsumerSecret("me47FzGbWYyqb2KkUiTy3Nwv059GCszbw0GTryZqlwAcfQkg0x");
            // アプリ＋ユーザー固有の情報
            builder.setOAuthAccessToken("3005552460-6xeOVmFtjMkHXdHZxeF5NSB3MkUJ8wpGNWvz4qb");
            builder.setOAuthAccessTokenSecret("9Z2WAvWFvufrONsel3C3oDDoevA4xUwkIPt64xepovTqb");

            //HTTPタイムアウト設定(ミリ秒)
            builder.setHttpConnectionTimeout(10000);
        }
        Configuration conf = builder.build();
        //TwitterFactoryをインスタンス化する
        TwitterFactory twitterFactory = new TwitterFactory(conf);
        //Twitterをインスタンス化する
        mTwitter = twitterFactory.getInstance();
    }
}