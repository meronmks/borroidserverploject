package com.meronmks.botroidserver.app;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class MySQLiteOpenHelper extends SQLiteOpenHelper {

    private Context context;

    public MySQLiteOpenHelper(Context c) {
        super(c, "/mnt/sdcard/external_sd/tenDB/ten_DB.db", null, 1);
        context = c;
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(context.getString(R.string.user_table));
        db.execSQL(context.getString(R.string.mention_table));
        db.execSQL(context.getString(R.string.timeline_table));
        db.execSQL(context.getString(R.string.routine_table));
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
